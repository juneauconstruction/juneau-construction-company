Juneau Construction Company, headquartered in Atlanta with an office in Miami, is one of the fastest-growing construction firms in the Southeast. Juneau was founded by Les & Nancy Juneau in 1997. Juneau performs as Construction Managers, Design-Builders and General Contractors – public and private.

Address: 3715 Northside Pkwy NW, Building 300, Suite 500, Atlanta, GA 30327, USA

Phone: 404-287-6000
